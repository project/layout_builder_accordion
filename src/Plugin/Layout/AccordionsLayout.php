<?php

namespace Drupal\layout_builder_accordion\Plugin\Layout;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Accordion layout.
 */
class AccordionsLayout extends LayoutDefault implements PluginFormInterface {

  /**
   * Builds configuration form.
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Administrative label'),
      '#default_value' => $this->configuration['label'],
    ];
    $form['check_always_open'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Open by default'),
      '#description' => $this->t('Allows to leave the accordions open by default'),
      '#default_value' => $this->configuration['check_always_open'] ?? FALSE,
    ];

    $form['independent_accordion'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Accordions can be opened regardless of the status of their siblings'),
      '#description' => $this->t('Allows to leave the accordions open by default'),
      '#default_value' => $this->configuration['independent_accordion'] ?? FALSE,
    ];

    return $form;
  }

  /**
   * Builds regions.
   */
  public function build(array $regions) {
    $build = parent::build($regions);
    return $build;
  }

  /**
   * Allows to save the configuration of the module.
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['check_always_open'] = $form_state->getValue('check_always_open');
    $this->configuration['independent_accordion'] = $form_state->getValue('independent_accordion');
  }

}
