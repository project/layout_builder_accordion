<?php

namespace Drupal\layout_builder_accordion\Plugin\Layout;

use Drupal\Core\Form\FormStateInterface;
use Drupal\layout_builder\Plugin\Layout\MultiWidthLayoutBase;

/**
 * Configurable two column layout plugin class.
 *
 * @internal
 *   Plugin classes are internal.
 */
class AccordionsTwoColumnLayout extends MultiWidthLayoutBase {

  /**
   * Accordion layout.
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['accordion_location'] = [
      '#type' => 'select',
      '#title' => $this
        ->t('Accordion location'),
      '#description' => $this->t('Choose the region where you want to render accordions per block.'),
      '#options' => [
        'first_region' => $this
          ->t('First region'),
        'second_region' => $this
          ->t('Second region'),
        'both_regions' => $this
          ->t('Both regions'),
      ],
      '#default_value' => $this->configuration['accordion_location'] ?? 'first_region',
    ];

    $form['check_always_open'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Always Open'),
      '#default_value' => $this->configuration['check_always_open'] ?? FALSE,
    ];

    $form['independent_accordion'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Accordions can be opened regardless of the status of their siblings'),
      '#description' => $this->t('Allows to leave the accordions open by default'),
      '#default_value' => $this->configuration['independent_accordion'] ?? FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function getWidthOptions() {
    return [
      '50-50' => '50%/50%',
      '33-67' => '33%/67%',
      '67-33' => '67%/33%',
      '25-75' => '25%/75%',
      '75-25' => '75%/25%',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['accordion_location'] = $form_state->getValue('accordion_location');
    $this->configuration['check_always_open'] = $form_state->getValue('check_always_open');
    $this->configuration['independent_accordion'] = $form_state->getValue('independent_accordion');

  }

  /**
   * {@inheritdoc}
   */
  public function build(array $regions) {
    $build = parent::build($regions);
    $layout_class = 'layout--twocol-section';
    $build['#attributes']['class'] = [
      'layout',
      $layout_class,
      $layout_class . '--' . $this->configuration['column_widths'],
    ];

    return $build;
  }

}
